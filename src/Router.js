import React from 'react';
import { Scene, Router, Actions } from 'react-native-router-flux';
import LoginForm from './components/LoginForm';
import SearchForm from './components/SearchForm';
import ForgotPassword from './components/ForgotPassword';
import SearchResults from './components/SearchResults';
import Camera from './components/Camera';
import AddVehicle from './components/AddVehicle';


const RouterComponent = () => {
  return (
    <Router sceneStyle={{ paddingTop: 0 }}>
    <Scene key="auth">
      <Scene hideNavBar key="login" component={LoginForm} />
        <Scene key="forgotpwd" component={ForgotPassword} title="Reset Password" />
    </Scene>
    <Scene key="main">
      <Scene
      key="searchform"
      component={SearchForm}
      title="Search Vehicles"
      rightTitle="Add"
      onRight={() => Actions.addVehicle()}
      initial
      />
      <Scene key="searchResults" component={SearchResults} title="Search Results" />

      </Scene>
      <Scene key="addVehicle" component={AddVehicle} title="Add Vehicle Details" />

      <Scene key="camera" component={Camera} />

    </Router>

  );
};
export default RouterComponent;

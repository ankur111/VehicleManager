import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children }) => {
  const { buttonStyle, textStyle } = styles;
  return (
    <TouchableOpacity onPress={onPress} style={buttonStyle}>
       <Text style={textStyle}>
         { children }
       </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'stretch',
    textAlign: 'center',
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
    paddingTop: 14,
    paddingBottom: 40,
    borderColor: '#ddd',
    borderRadius: 20,
    backgroundColor: '#34e59b'
  },
  buttonStyle: {
    backgroundColor: '#3c4558',
    borderColor: 'white',
    borderRadius: 25,
    overflow: 'hidden',
    marginRight: 35,
    marginLeft: 35,
    marginTop: 10,
    height: 50,

}

};

export { Button };

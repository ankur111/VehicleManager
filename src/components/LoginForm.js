import React, { Component } from 'react';
import { Image, Text, View, TouchableOpacity, Linking } from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, passwordChanged, loginUser } from '../actions';
import { CardSection, Input, Button, Spinner } from './common';


class LoginForm extends Component {
  onEmailChange(text) {
    this.props.emailChanged(text);
}
onPasswordChange(text) {
  this.props.passwordChanged(text);
}
onButtonPress() {
  const { email, password } = this.props;
  this.props.loginUser({ email, password });
}
forgotpwd() {
 Linking.openURL('https://fleetmanager.stems-solutions.com/password/reset');
}
renderError() {
  if (this.props.error) {
    return (
      <View style={{ backgroundColor: '#fff' }}>
        <Text style={styles.errorTextStyle}>
          {this.props.error}
        </Text>
      </View>
    );
  }
}
renderButton() {
  if (this.props.loading) {
    return <Spinner size="large" />;
  }
  return (
    <Button onPress={this.onButtonPress.bind(this)}>
        login
    </Button>
  );
}

  render() {
    return (
      <View style={{ paddingTop: 80, backgroundColor: '#3c4558', flex: 1 }}>
      <Image
      style={styles.logoImageStyle}
      source={require('../assets/FleetManLogo.png')}
      // source={{ uri: 'https://facebook.github.io/react/logo-og.png' }}
      />
      <Text style={styles.logoTextStyle}> FLEET MANAGER </Text>
        <CardSection>
          <Input
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange.bind(this)}
            value={this.props.email}
          />
        </CardSection>

        <CardSection>
        <Input
        secureTextEntry
        placeholder="Password"
        onChangeText={this.onPasswordChange.bind(this)}
        value={this.props.password}
        style={styles.inputTextStyle}
        />
        </CardSection>
          {this.renderError()}

          {this.renderButton()}
          <TouchableOpacity onPress={this.forgotpwd} style={styles.logoTextStyle}>
          <Text > Forgot Password? </Text>
          </TouchableOpacity>

            <Text style={styles.textPoweredBy}> Powered by Stems Solutions (P) Ltd </Text>
      </View>
    );
  }

}

const styles = {
  logoTextStyle: {
    alignSelf: 'center',
    marginBottom: 70,
    paddingTop: 10,
  },
  logoImageStyle: {
    alignSelf: 'center',
    height: 100
  },
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'stretch',
    textAlign: 'center',
    color: 'red',
    backgroundColor: '#3c4558'
  },
  textPoweredBy: {
    alignSelf: 'center',
    fontSize: 12,
    paddingTop: 20,
    color: '#a9a9a9'
  },
  inputTextStyle: {
    paddingBottom: 10,
  }
};
const mapStateToProps = state => {
  return {
    email: state.auth.email,
    password: state.auth.password,
    error: state.auth.error,
    loading: state.auth.loading
  };
};
export default connect(mapStateToProps, {
  emailChanged, passwordChanged, loginUser
})(LoginForm);

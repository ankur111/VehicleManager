import React, { Component } from 'react';
import { View, Picker, Text } from 'react-native';
import DatePicker from 'react-native-datepicker';
import { connect } from 'react-redux';
import { requiredToChanged,
          requiredTimeChanged,
          search,
          vehicleTypeChanged,
          requiredFromChanged,
          durationOfHireChanged,
          inputTimeChanged } from '../actions';
import { CardSection, Spinner, Button } from './common';

const today = new Date();

class SearchForm extends Component {
onVechTypeChange(text) {
  this.props.vehicleTypeChanged(text);
}
onReqFromChange(text) {
  this.props.requiredFromChanged(text);
}
onReqToChange(text) {
  this.props.requiredToChanged(text);
}
onDurationChange(text) {
  this.props.durationOfHireChanged(text);
}

  onTimeChange() {
    const value = this.props.reqTime;
    this.props.inputTimeChanged({ value });
  }
  onButtonPress() {
    const token = this.props.token;
    const vehicleType = this.props.vehicletype;
    const reqTodate = this.props.reqTodate;
    const reqfromdate = this.props.reqfromdate;
    this.props.search({ token, vehicleType, reqTodate, reqfromdate });
  }
  renderButton() {
      if (this.props.loading) {
        return <Spinner size="large" />;
      }
      return (
        <Button onPress={this.onButtonPress.bind(this)}>
            Search Vehicles
        </Button>
      );
    }

  render() {
      return (
        <View style={{ paddingTop: 80, backgroundColor: '#3c4558', flex: 1 }}>

        <Text style={styles.logoTextStyle}> Find Vehicles For Hire </Text>

          <CardSection>
            <Picker
            style={{ flex: 1 }}
            selectedValue={this.props.vehicletype}
            onValueChange={text => this.props.vehicleTypeChanged({ prop: 'vehicletype', value: text })}
            >
            <Picker.Item label="Car" value="Car" />
            <Picker.Item label="Jeep" value="Jeep" />
            <Picker.Item label="Van" value="Van" />
            <Picker.Item label="Bus" value="Bus" />
            </Picker>
          </CardSection>

          <CardSection>
            <DatePicker
              style={{ width: 200 }}
              date={this.props.reqfromdate}
              mode="datetime"
              placeholder="REQUIRED FROM"
              format="MMMM Do YYYY, h:mm a"
              is24Hour
              minDate={today}
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
              dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
              },
              dateInput: {
              marginLeft: 36
              }
              // ... You can check the source to find the other keys.
              }}
              onDateChange={(datetime) => this.props.requiredFromChanged({
              prop: 'reqfromdate', value: datetime })}
            />
          </CardSection>

          <CardSection>

            <DatePicker
              style={{ width: 200 }}
              date={this.props.reqTodate}
              mode="datetime"
              placeholder="REQUIRED TO"
              format="MMMM Do YYYY, h:mm a"
              is24Hour
              confirmBtnText="Confirm"
              cancelBtnText="Cancel"
              customStyles={{
              dateIcon: {
              position: 'absolute',
              left: 0,
              top: 4,
              marginLeft: 0
              },
              dateInput: {
              marginLeft: 36
              }
              // ... You can check the source to find the other keys.
              }}
              onDateChange={(datetime) => this.props.requiredToChanged({
              prop: 'reqTodate', value: datetime })}
            />
          </CardSection>
          {this.renderButton()}

        </View>
      );
    }
}
const styles = {
  logoTextStyle: {
    alignSelf: 'center',
    marginBottom: 20,
    paddingTop: 10,
    fontSize: 22,
    color:'#f5f5f5'
  },
  logoImageStyle: {
    alignSelf: 'center',
    height: 100
  },
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'stretch',
    textAlign: 'center',
    color: 'red',
    backgroundColor: '#3c4558'
  },
  textPoweredBy: {
    alignSelf: 'center',
    fontSize: 12,
    paddingTop: 20,
    color: '#a9a9a9'
  },
  inputTextStyle: {
    paddingBottom: 10
  }
};

const mapStateToProps = (state) => {
  const { token } = state.auth;
  const { reqTodate, vehicletype, reqfromdate, loading } = state.search;
  return { token, loading, reqTodate, vehicletype, reqfromdate };
};

export default connect(mapStateToProps,
  { requiredTimeChanged,
    requiredToChanged,
    vehicleTypeChanged,
    requiredFromChanged,
    durationOfHireChanged,
    search,
    inputTimeChanged })(SearchForm);

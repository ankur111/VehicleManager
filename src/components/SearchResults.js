import React, { Component } from 'react';
import { FlatList, Text, View, Alert, ActivityIndicator, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Card, CardSection, Button } from './common';

class SearchResults extends Component {

  constructor(props){
    super(props);
    this.state = {
    isLoading: true
  }
  }

  componentDidMount() {
    const responseJson = this.props.data.vehicles;
    this.setState({
      isLoading: false,
      dataSource: responseJson
  });
  console.log('The received data: ');
  console.log(responseJson);
     }

FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 10,
          width: "100%",
          backgroundColor: "#ffffff",
        }}
      />
    );
  }
  onButtonPress() {
    Actions.addVehicle();
  }
  GetFlatListItem (fruit_name) {

  Alert.alert(fruit_name);

  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }

    return (

      <View style={styles.MainContainer}>

        <FlatList
          data={this.state.dataSource}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item }) =>
          <View>

            <View style={styles.SubContainer}>
              <View style={{ flex: 1 }}>
                <Image
                style={styles.imageStyle}
                source={{ uri: item.image }}
                />
              </View>
              <View style={styles.TextsContainer}>
                <Text style={styles.HeadingTextStyle}>{item.manufacturer}</Text>
                <Text style={styles.TextStyle}>{`Reg: ${item.reg_number}`}</Text>
                <Text style={styles.TextStyle}>{`Seating Capacity: ${item.capacity}`}</Text>
              </View>
            </View>

              <View>
                <Button onPress={this.onButtonPress.bind(this)}>
                  View Details
                </Button>
              </View>

          </View>
          }
          keyExtractor={(item, id) => id.toString()}
        />

      </View>

    );
  }
}


const styles = {
  MainContainer: {
    flex: 1,
    paddingTop: 56,
  },
  SubContainer: {
    paddingTop: 10,
    paddingBottom: 25,
    marginTop: 10,
    flexDirection: 'row',
  },
  TextsContainer: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'space-between'
  },
  HeadingTextStyle: {
    fontSize: 20,
    color: '#111',
    textAlign: 'left',
    paddingLeft: 20
  },
  TextStyle: {
    fontSize: 16,
    color: '#111',
    textAlign: 'left',
    paddingLeft: 20

  },
  imageStyle: {
    height: 150,
    flex: 1,
    padding: 5
  }
};

export default SearchResults;

import {
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  RESET_PASSWORD,
  FORGOT_EMAIL_CHANGED,
  FORGOT_PASSWORD_CHANGED,
  FORGOT_PASSWORD_CONFIRM_CHANGED,
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER
} from '../actions/types';

const INITIAL_STATE = {
  forgotemail: '',
  forgotpassword: '',
  forgotconpassword: '',
  resetpasswordloading: '',
  email: '',
  password: '',
  user: null,
  token: '',
  error: '',
  loading: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case EMAIL_CHANGED:
      return { ...state, email: action.payload };
    case FORGOT_EMAIL_CHANGED:
      return { ...state, forgotemail: action.payload };
    case FORGOT_PASSWORD_CHANGED:
      return { ...state, forgotpassword: action.payload };
    case FORGOT_PASSWORD_CONFIRM_CHANGED:
      return { ...state, forgotconpassword: action.payload };
    case PASSWORD_CHANGED:
      return { ...state, password: action.payload };
    case LOGIN_USER_SUCCESS:
      return { ...state, token: action.payload, error: '', loading: false };
    case LOGIN_USER_FAIL:
      return { ...state, error: 'Authentication Failed', password: '', loading: false };
    case LOGIN_USER:
      return { ...state, loading: true, error: '' };
    case RESET_PASSWORD:
      return { ...state, resetpasswordloading: true, error: '' };
    case RESET_PASSWORD_SUCCESS:
      return { ...state, resetpasswordloading: false, error: '' };
    case RESET_PASSWORD_FAILURE:
      return { ...state, resetpasswordloading: false, error: '' };
    default:
      return state;
  }
};

import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import SearchReducer from './SearchReducer';
import AddVehicleForm from './AddVehicleForm';

export default combineReducers({
  auth: AuthReducer,
  search: SearchReducer,
  addvehicle: AddVehicleForm,
});

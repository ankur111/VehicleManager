import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import {
  SEARCH,
  SEARCH_SUCCESS,
  VEHICLE_TYPE_CHANGED,
  REQUIRED_FROM_CHANGED,
  DURATION_OF_HIRE_CHANGED,
  REQUIRED_TO_CHANGED
 } from './types';


 export const vehicleTypeChanged = ({ prop, value }) => {
   return {
     type: VEHICLE_TYPE_CHANGED,
     payload: { prop, value }
   };
 };

 export const requiredFromChanged = ({ value }) => {
   return {
     type: REQUIRED_FROM_CHANGED,
     payload: { value }
   };
 };

 export const requiredToChanged = ({ value }) => {
   return {
     type: REQUIRED_TO_CHANGED,
     payload: { value }
   };
 };


 export const durationOfHireChanged = (text) => {
   return {
     type: DURATION_OF_HIRE_CHANGED,
     payload: text
   };
 };

 export const search = ({ token, vehicleType, reqTodate, reqfromdate }) => {
   return (dispatch) => {
     dispatch({ type: SEARCH });

     axios({
     method: 'post',
     url: 'https://fleetmanager.stems-solutions.com/search',
     headers: {
       Accept: 'application/json',
      Authorization: `Bearer ${token}`,
     },
     data: {
       Vehicle: vehicleType,
       DateTo: reqTodate,
       DateFrom: reqfromdate
     }
   })
   .then(responseJson => searchSuccess(dispatch, responseJson))
   .catch((error) => console.log(error));
 };
 };
export const searchSuccess = (dispatch, responseJson) => {
  dispatch({
    type: SEARCH_SUCCESS,
  });
  console.log(responseJson);
    Actions.searchResults(responseJson);
};

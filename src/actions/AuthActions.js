import axios from 'axios';
import { Actions } from 'react-native-router-flux';
import {
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  FORGOT_EMAIL_CHANGED,
  FORGOT_PASSWORD_CHANGED,
  FORGOT_PASSWORD_CONFIRM_CHANGED,
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER
 } from './types';

export const emailChanged = (text) => {
  return {
    type: EMAIL_CHANGED,
    payload: text
  };
};
export const forgotemailChanged = (text) => {
  console.log(text);
  return {
    type: FORGOT_EMAIL_CHANGED,
    payload: text
  };
};
export const forgotPassChanged = (text) => {
  console.log(text);
  return {
    type: FORGOT_PASSWORD_CHANGED,
    payload: text
  };
};
export const forgotConPassChanged = (text) => {
  console.log(text);
  return {
    type: FORGOT_PASSWORD_CONFIRM_CHANGED,
    payload: text
  };
};

export const passwordChanged = (text) => {
  return {
    type: PASSWORD_CHANGED,
    payload: text
  };
};
export const loginUser = ({ email, password }) => {
  return (dispatch) => {
    dispatch({ type: LOGIN_USER });

    axios({
    method: 'post',
    url: 'https://fleetmanager.stems-solutions.com/oauth/token',
    data: {
      username: email,
      password,
      grant_type: 'password',
      client_id: '4',
      client_secret: 'uMylsqMhByou8HorOCb6nvZxQTdmAJtfbHJLsAsP',
      scope: '*'
    }
  }).then(response => loginUserSuccess(dispatch, response))
  .catch(error => loginUserFail(dispatch, error));
};
};

const loginUserFail = (dispatch, error) => {
  dispatch({ type: LOGIN_USER_FAIL });
};

const loginUserSuccess = (dispatch, response) => {
  dispatch({
    type: LOGIN_USER_SUCCESS,
    payload: response.data.access_token
  });
  Actions.main();
};

export const forgotPassword = ({ forgotemail, forgotpassword, forgotconpassword }) => {
return (dispatch) => {
dispatch({ type: RESET_PASSWORD });

  axios({
  method: 'post',
  url: 'https://fleetmanager.stems-solutions.com/password/email',
  data: {
    email: forgotemail,
  }
}).then(response => {
  axios({
  method: 'post',
  url: 'https://fleetmanager.stems-solutions.com/password/reset',
  data: {
    email: forgotemail,
    password: forgotpassword, //need to add
    password_confirmation: forgotconpassword, //need to add
    token: response.data.token
  }
}).then(resetPasswordSuccess(dispatch)).catch(() => console.log(response));
}).catch(() => console.log('Something went wrong. Please try again.'));
};
};

const resetPasswordSuccess = (dispatch) => {
  dispatch({
    type: RESET_PASSWORD_SUCCESS,
  });
  Actions.main();
};


//once a token received
//send back the token and email id
//once this is sucess then only fire resetPasswordSuccess
